#!/bin/bash


echo 'var tableChar = [];' > table.js

for LETTER_MP in letters/*.mp; do
      lineChar=$(sed -n '/charstart/p' $LETTER_MP)
      keyCode=$( echo $lineChar | sed -e 's/.*charstart(\(.*\))/\1/')
      IFS=.
      set $LETTER_MP
      LETTER_=$1
      IFS=/
      set $LETTER_
      LETTER=$2
      IFS=_
      set $LETTER
      LETTER=$2
      CATEGORIE=$1
      IFS=
      echo '    tableChar['$keyCode'] = ["'$CATEGORIE'", "'$LETTER'"];' >> table.js
done
