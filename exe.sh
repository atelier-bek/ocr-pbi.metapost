#!/bin/bash

#boucle de Mpost mp en svg
# rm svg/*.svg
# bash single.sh -all

for LETTER_svg in svg/*.svg
do
  IFS=.
  set $LETTER_svg
  LETTER_=$1
  IFS=/
  set $LETTER_
  LETTER=$2
  IFS=
  echo $LETTER

  sed '/stroke:rgb(100.000000%,0.000000%,0.000000%)/d' $LETTER_svg > svg/clean/$LETTER.svg
  
  # cp $LETTER_svg svg/clean/
     inkscape --verb EditSelectAllInAllLayers \
              --verb SelectionUnGroup \
              --verb StrokeToPath \
              --verb SelectionUnion \
              --verb FileSave \
              --verb FileClose \
              --verb FileQuit \
     svg/clean/$LETTER.svg
done

# boucle python importation des svg dans le otf.
# python importSVG.py

#les remove
rm *.log 
