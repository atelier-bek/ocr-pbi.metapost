# /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import glob
import sys

# lancer dans le terminal "python importSVG.py name"
NameFile = sys.argv[1]

#ici il faut créer un fichier fontforge(.sfd) vide sans rien dedans.
font = fontforge.open('temp/font.sfd')
#Ici il faut mettre le dossier avec tout tes svg et les nommer par exmple 65.svg pour le A. Doc sur unicode-table.com.
svg_final = glob.glob('svg/*svg')

for letter_svg in svg_final:
    #ici ça split le nom du svg pour garder que le numéro du caractère
    letter = letter_svg.split("/")[-1].replace(".svg", "")
    letter = letter.split("_")[-1].replace(".svg", "")
    print(letter)

    #ici on ouvre la case du caractère
    glyph = font.createChar( unicode(letter))
    #ici on attribu une chasse à la lettre
    glyph.width = 611
    #on import le svg
    glyph.importOutlines(letter_svg)
    glyph.importOutlines(letter_svg)

#on nom la font et on genère
font.fontname = 'font-'+NameFile
font.familyname = 'font'
font.generate('final/font-'+NameFile+'.sfd')
font.generate('final/font-'+NameFile+'.ttf')
font.generate('final/font-'+NameFile+'.otf')
