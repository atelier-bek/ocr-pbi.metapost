#!/bin/bash
MOT=$1
for ((;;));
do
  DATE=$(date +"%d-%m-%Y-%T")
  convert testing.jpg -resize x700 archive/$DATE.jpg

  COMPOSE=()
   
  for LETTER in $MOT
  do
    mpost -interaction=batchmode -s 'outputformat="svg"' letters/$LETTER.mp
    mv $LETTER*.svg svg/$LETTER.svg
    COMPOSE+=('svg/'$LETTER'.svg')
  done

  echo ${COMPOSE[@]}
  montage ${COMPOSE[@]} \
  -mode Concatenate -tile x1  testing.jpg
  rm *.log
sleep 5;
done
